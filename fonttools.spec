# open this flag when support more pkg
%bcond_with tests

%global pypi_name fonttools
%global common_description %{expand:
fontTools is a library for manipulating fonts, written in Python. The project includes the TTX tool, that can
convert TrueType and OpenType fonts to and from an XML text format, which is also called TTX. It supports TrueType,
OpenType, AFM and to an extent Type 1 and some Mac-specific formats.}

Summary:        Tools to manipulate font files
Name:           fonttools
Version:        4.49.0
Release:        5%{?dist}
License:        MIT
URL:            https://github.com/fonttools/fonttools/
Source0:        https://github.com/%{name}/%{name}/archive/%{version}.tar.gz

Requires:       python3-fonttools python3-setuptools
BuildArch:      noarch
Provides:       ttx = %{version}-%{release}

%description
%{common_description}

%package -n python3-fonttools
Summary:        Python 3 fonttools library
BuildRequires:  python3-devel python3-setuptools python3-setuptools_scm

%if %{with tests}
BuildRequires:  python3-zopfli python3-pytest python3-brotli python3-munkres python3-scipy python3-fs
%endif

Requires:       python3-brotli python3-munkres python3-lxml python3-scipy python3-fs

BuildArch:      noarch

%description -n python3-fonttools
%{common_description}

%prep
%autosetup -n %{name}-%{version} -p1
rm -rf *.egg-info
sed -i '1d' Lib/fontTools/mtiLib/__init__.py

%build
%py3_build

%install
%{__python3} setup.py install --skip-build --root %{buildroot}

%check
%if %{with tests}
PYTHONPATH=%{buildroot}%{python3_sitelib} %{python3} -m pytest --ignore Tests/otlLib/optimize_test.py
%endif

%files
%{_bindir}/pyftmerge
%{_bindir}/pyftsubset
%{_bindir}/ttx
%{_bindir}/fonttools
%{_mandir}/man1/ttx.1*

%files -n python3-fonttools
%license LICENSE
%doc NEWS.rst README.rst
%{python3_sitelib}/fontTools
%{python3_sitelib}/%{name}-%{version}-py%{python3_version}.egg-info

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.49.0-5
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Sat Sep 14 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.49.0-4
- [Type] other
- [DESC] Remove subpkg python3-fonttools+ufo

* Thu Aug 29 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.49.0-3
- [Type] other
- [DESC] Add subpkg python3-fonttools+ufo

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.49.0-2
- Rebuilt for loongarch release

* Tue Feb 20 2024 Upgrade Robot <upbot@opencloudos.org> - 4.49.0-1
- Upgrade to version 4.49.0
- Fix CVE-2023-45139

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.42.0-3
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.42.0-2
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Aug 21 2023 Shuo Wang <abushwang@tencent.com> - 4.42.0-1
- update to 4.42.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.30.0-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.30.0-2
- Rebuilt for OpenCloudOS Stream 23

* Tue Jan 31 2023 Shuo Wang <abushwang@tencent.com> - 4.30.0-1
- initial build

